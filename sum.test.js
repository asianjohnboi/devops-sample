const sum = require('./sum');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('add 2 + 4 to equal 6', () => {
    expect(sum(2, 4)).toBe(6);
});

test('add -2 + 4 to equal 2', () => {
  expect(sum(-2, 4)).toBe(2);
});